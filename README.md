# Application vide PHP, MySQL et WET-BOEW

## Développement

### Base de données

`docker run --name sample-php-db -v sample-php-db-data:/var/lib/mysql -e MYSQL_DATABASE=sample-php -e MYSQL_USER=sample-php -e MYSQL_PASSWORD=password -e MYSQL_ROOT_PASSWORD=root_password -d mysql:5`

### Application Web (Dockerfile)

`docker build -t sample-php .`

`docker run --name sample-php-web --link sample-php-db:mysql -p 80:80 -v $(pwd):/var/www/html -d sample-php`

### Tests

#### Gitlab Auto CI

[Dependency Scanning](https://docs.gitlab.com/ee/user/application_security/dependency_scanning/index.html)

[Static Application Security Testing](https://docs.gitlab.com/ee/user/application_security/sast/index.html)

#### Composer

Exécute [PHP-Parallel-Lint](https://github.com/JakubOnderka/PHP-Parallel-Lint) et [PHP_CodeSniffer](https://github.com/squizlabs/PHP_CodeSniffer)

`docker run -v $(pwd):/app -it --rm composer install`

`docker run -v $(pwd):/app -it --rm composer composer test`

#### Node

Exécute [Markdownlint](https://github.com/igorshubovych/markdownlint-cli)

`docker run -v $(pwd):/usr/node/app -w /usr/node/app -it --rm node npm install`

`docker run -v $(pwd):/usr/node/app -w /usr/node/app -it --rm node npm test`

#### Gitlab Runner

Avec [Gitlab Runner](https://docs.gitlab.com/runner/)

`gitlab-runner exec docker composer-test`

`gitlab-runner exec docker npm-test`

[NOT WORKING](https://gitlab.com/gitlab-org/gitlab-runner/issues/2438)! `docker run -it --rm -v $(pwd):/code -w /code -v /var/run/docker.sock:/var/run/docker.sock gitlab/gitlab-runner exec docker composer-test`
